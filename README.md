# osu-updater

This is a simple shell script that builds the latest version of osu-lazer for Linux and installs it in /usr/local.

It is intended for Linux noobs, so it lacks some customizability.

The installation procedures and resources are based heavily on the Arch pkgbuild at https://aur.archlinux.org/packages/osu-lazer/.

# Installation

Download the `osu-updater` script from this repository and place it somewhere on your path.

**Note:** This script does not update itself automatically! If I update this repository, you should update your copy of the script manually.
If you wish, you may symbolically link your copy to ~/.osu-updater/linux-resources/osu-updater and it will automatically update itself each time it is run, but this has some issues. (The new version won't run until it is run again.)
**Note 10/27/2019: I fixed a bug preventing it from working in 2-digit months. Sorry about that!**

# Usage

Just run `osu-updater`. It will build osu-lazer in `$HOME/.osu-updater` and install it on your system.

To uninstall osu-lazer, run `osu-updater --uninstall` as root. You might also want to remove the build cache directory, `.osu-updater`, located at the top level of your home directory. If you remove it from the terminal, I recommend using `rm -rf` unless you want to manually instruct `rm` to delete each and every write-protected git file.
(Obviously, to uninstall the updater script, just delete it.)
