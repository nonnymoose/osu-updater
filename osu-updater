#!/bin/bash
set -e

true

MY_LOC="$(realpath "$0")"

build_osu() {
	if [[ "$(whoami)" == "root" ]]; then
		echo "This script caches its builds in your home directory." 1>&2
		echo "In general, it's a really bad idea to run such things as root, because they could modify files in your home directory to no longer be owned by you and break other programs." 1>&2
		echo "Please re-run this script as yourself." 1>&2
		exit 1
	fi

	which git &>/dev/null || { echo "Missing `git` command"; exit 2; }
	which dotnet &>/dev/null || { echo "Missing `dotnet` command"; exit 2; }
	DOTNET_VERSION="$(dotnet --version | sed -r 's/([[:digit:]]+\.[[:digit:]]).*$/\1/')"

	if [[ ! -d "$STORAGE_LOC" ]]; then
		mkdir "$STORAGE_LOC" || { echo "Could not create $STORAGE_LOC" 1>&2; exit 1; }
		echo "Storing builds in $STORAGE_LOC" 1>&2
		echo "If you remove this directory from the terminal, use rm -rf. Thank me later." 1>&2
		sleep 1 # so that the user can read the previous message
		git clone "https://github.com/ppy/osu" "$STORAGE_LOC/osu-git"
		git clone "https://gitlab.com/nonnymoose/osu-updater" "$STORAGE_LOC/linux-resources"
	else
		echo "Builds stored in $STORAGE_LOC" 1>&2
		cd "$STORAGE_LOC/osu-git"
		git fetch
		cd "$STORAGE_LOC/linux-resources"
		git pull
	fi

	cd "$STORAGE_LOC/osu-git"
	CURRENT_VERSION="$(git tag -l | sed -nr '/^[[:digit:]]{4}\.[[:digit:]]{3,4}\.[[:digit:]]/p' | sort -rV | head -n1)"
	CURRENT_VERSION_SORT="$(echo "$CURRENT_VERSION_SORT" | sed -r 's/\.([[:digit:]]{3})\./\.0\1\./')"
	if [[ -e "$STORAGE_LOC/cached_version" ]]; then
		PREV_INSTALLED_VERSION="$(<"$STORAGE_LOC/cached_version")"
	else
		PREV_INSTALLED_VERSION=0000.0000.0
	fi

	if [[ "$(sort -rV <(echo -e "$PREV_INSTALLED_VERSION\n$CURRENT_VERSION") | head -n1)" == "$PREV_INSTALLED_VERSION" ]]; then
		echo "The version installed on your machine is already up-to-date." 1>&2;
		exit 3
	fi

	echo "Building osu!-lazer..." 1>&2
	sleep 1
	git checkout "$CURRENT_VERSION"
	dotnet publish          osu.Desktop                 \
					--framework         netcoreapp$DOTNET_VERSION   \
					--configuration     Release                     \
					--runtime           linux-x64                   \
					--self-contained    false                       \
					/property:Version="$CURRENT_VERSION"						\
					--output            ./bin/Release/netcoreapp$DOTNET_VERSION/linux-x64
	echo "Installing osu!-lazer..." 1>&2
	sleep 1
	sudo "$MY_LOC" --install "$STORAGE_LOC" "$STORAGE_LOC/osu-git/osu.Desktop/bin/Release/netcoreapp$DOTNET_VERSION/linux-x64"
	echo "$CURRENT_VERSION" > "$STORAGE_LOC/cached_version"
	echo "Done!" 1>&2
}

install_osu() {
	# set -x
	if [[ "$(whoami)" != "root" ]]; then
		echo "The installer must be run as root." 1>&2
		exit 1
	fi
	cd "$STORAGE_LOC/linux-resources"
	mkdir -p "/usr/local/bin"
	install -m755 'osu-launcher' "/usr/local/bin/osu-lazer"
	mkdir -p "/usr/local/share/mime/packages"
	install -m644 "x-osu-lazer.xml" "/usr/local/share/mime/packages/x-osu-lazer.xml"
	mkdir -p "/usr/local/share/applications"
	install -m644 "osu-lazer.desktop" "/usr/local/share/applications/osu-lazer.desktop"
	mkdir -p "/usr/local/share/pixmaps"
	install -m644 "osu-lazer.png" "/usr/local/share/pixmaps/osu-lazer.png"
	cd "$BUILT_LOC"
	mkdir -p /usr/local/lib/osu-lazer
	shopt -s nullglob
	for binary in {*.so,*.dll,*.json,*.pdb}; do
		install -m755 "$binary" "/usr/local/lib/osu-lazer/$binary"
	done
	cd x86_64
	for binary in {*.so,*.dll,*.json,*.pdb}; do
		install -m755 "$binary" "/usr/local/lib/osu-lazer/$binary"
	done
	shopt -u nullglob
}

uninstall_osu() {
	if [[ "$(whoami)" != "root" ]]; then
		echo "The uninstaller must be run as root." 1>&2
		exit 1
	fi
	rm /usr/local/bin/osu-lazer /usr/local/share/mime/packages/x-osu-lazer.xml /usr/local/share/applications/osu-lazer.desktop /usr/local/share/pixmaps/osu-lazer.png
	rm -r /usr/local/lib/osu-lazer/
}

if [[ $# -eq 3 && $1 == "--install" ]]; then
	STORAGE_LOC="$2"
	BUILT_LOC="$3"
	install_osu
elif [[ $# -eq 1 && $1 == "--uninstall" ]]; then
	if [[ "$(whoami)" == "root" ]]; then
		uninstall_osu
	else
		sudo "$MY_LOC" --uninstall
	fi
else
	STORAGE_LOC="$HOME/.osu-updater"
	build_osu
fi
